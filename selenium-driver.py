# selenium-driver.py
import pickle
from selenium import webdriver
import os

class SeleniumDriver(object):
    def __init__(
        self,
        # pickle file path to store cookies
        cookies_file_path='./tmp/chrome/cookies.pkl',
        # list of websites to reuse cookies with
        cookies_websites=["https://portal.create.tv"]
    ):
        self.cookies_file_path = cookies_file_path
        self.cookies_websites = cookies_websites

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--incognito")
        chrome_options.add_argument("--headless")

        self.driver = webdriver.Remote(
            command_executor='http://127.0.0.1:4444/wd/hub',
            options=chrome_options)

    def load_cookie(self):
        try:
            # load cookies for given websites
            cookies = pickle.load(open(self.cookies_file_path, "rb"))
            for website in self.cookies_websites:
                self.driver.get(website)
                for cookie in cookies:
                    self.driver.add_cookie(cookie)
                self.driver.refresh()
        except Exception as e:
            # it'll fail for the first time, when cookie file is not present
            print(str(e))
            print("Error loading cookies")

    def save_cookies(self):
        # save cookies
        cookies = self.driver.get_cookies()
        pickle.dump(cookies, open(self.cookies_file_path, "wb"))

    def test_daily_performance(self):
        try:
            # load cookies for given websites
            cookies = pickle.load(open(self.cookies_file_path, "rb"))
            for website in self.cookies_websites:
                self.driver.get(website)
                for cookie in cookies:
                    self.driver.add_cookie(cookie)
                self.driver.refresh()
        except Exception as e:
            # it'll fail for the first time, when cookie file is not present
            print(str(e))
            print("Error loading cookies")

        url = "https://portal.create.tv/performance/youtube/overview"
        self.driver.get(url)
        print(self.driver.current_url)
        assert self.driver.current_url == url

    def close_all(self):
        # close all open tabs
        if len(self.driver.window_handles) < 1:
            return
        for window_handle in self.driver.window_handles[:]:
            self.driver.switch_to.window(window_handle)
            self.driver.close()

    def quit(self):
        self.save_cookies()
        self.close_all()
        self.driver.quit()


def is_app_logged_in(self):
    driver.get("https://portal.create.tv")
    if driver.current_url == "https://portal.create.tv/signin":
        return False
    else:
        return True


def app_login(username, password):
    driver.find_element_by_xpath("//*[@id='email']").send_keys(username)
    driver.find_element_by_xpath("//*[@id='password']").send_keys(password)
    driver.find_element_by_xpath("/html/body/div[1]/div/div[2]/div[2]/div[1]/form/div[5]/button").click()
    return driver

if __name__ == '__main__':
    """
    Run  - 1
    First time authentication and save cookies

    Run  - 2
    Reuse cookies and use logged-in session
    """
    selenium_object = SeleniumDriver()
    driver = selenium_object.driver
    username = os.getenv('APP_USERNAME')
    password = os.getenv('APP_PASSWORD')

    # if is_app_logged_in(driver):
    #     print("Already logged in")
    # else:
    #     print("Not logged in. Attempting login")
    #     app_login(username, password)

    # selenium_object.test_daily_performance()
    selenium_object.quit()