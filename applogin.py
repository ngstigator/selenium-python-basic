import pickle
import os
from selenium import webdriver

def app_login(username, password):

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--incognito")
    chrome_options.add_argument("--headless")

    driver = webdriver.Remote(
        command_executor='http://127.0.0.1:4444/wd/hub',
        options=chrome_options)

    driver.get("https://portal.create.tv/signin")
    driver.find_element_by_xpath("//*[@id='email']").send_keys(username)
    driver.find_element_by_xpath("//*[@id='password']").send_keys(password)
    driver.find_element_by_xpath("/html/body/div[1]/div/div[2]/div[2]/div[1]/form/div[5]/button").click()
    return driver

if __name__ == '__main__':

    username = os.getenv('APP_USERNAME')
    password = os.getenv('APP_PASSWORD')
    login_driver = app_login(username, password)