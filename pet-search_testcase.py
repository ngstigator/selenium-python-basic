from selenium import webdriver

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--incognito")
# chrome_options.add_argument("--headless")

driver = webdriver.Chrome(options=chrome_options)

# driver = webdriver.Remote(
#     command_executor='http://127.0.0.1:4444/wd/hub',
#     options=chrome_options)

# PETCUREAN
search_terms = 'kimchi'

driver.set_window_size(960, 960)

# go to Petcurean web site
driver.get('https://www.petcurean.com/')

# toggle search field
search_toggle = driver.find_element_by_class_name('search-toggle')
search_toggle.click()

# populate search field
search_field = driver.find_element_by_name('s')
search_field.send_keys(search_terms)

# submit search
search_button = driver.find_element_by_class_name('search-submit')
search_button.click()

# verify no results
search_results = driver.find_element_by_class_name('results-count').text
assert 'We found 0 results' in search_results
print(search_results)

driver.close()
driver.quit()