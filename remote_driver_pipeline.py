import os
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
# comment if using chrome_options
# from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--headless")

driver = webdriver.Remote(
    command_executor='http://127.0.0.1:4444/wd/hub',
    options=chrome_options)

# driver = webdriver.Remote(
#     command_executor='http://127.0.0.1:4444/wd/hub',
#     desired_capabilities=DesiredCapabilities.CHROME)

try:
    driver.get('https://chrisng.ca/')
except WebDriverException:
    print("page down")

assert "Chris" in driver.title

print(driver.title)

# print( '\n'.join([f'{k}: {v}' for k, v in sorted(os.environ.items())]) )
if os.getenv('TEST_ENV_VAR') == 'secret':
    print('env var match')
else:
    print('no match')

driver.quit()