import pickle
import os
from selenium import webdriver

class AppLogin(object):
    def __init__(
        self,
        cookies_filepath='./tmp/chrome/cookies.pkl',
        cookies_websites=["https://portal.create.tv"]
    ):
        self.cookies_filepath = cookies_filepath
        self.cookies_websites = cookies_websites

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--incognito")
        chrome_options.add_argument("--headless")

        self.driver = webdriver.Remote(
            command_executor='http://127.0.0.1:4444/wd/hub',
            options=chrome_options)

        if os.path.exists("./tmp/chrome/cookies.pkl"):
            self.load_cookies()
        else:
            self.driver.get("https://portal.create.tv")
            self.app_login(username, password)
            self.save_cookies()
            self.load_cookies()

        # return self.driver

    def save_cookies(self):
        # save cookies
        cookies = self.driver.get_cookies()
        pickle.dump(cookies, open(self.cookies_filepath, "wb"))

    def load_cookies(self):
        try:
            # load cookies for given websites
            cookies = pickle.load(open(self.cookies_filepath, "rb"))
            for website in self.cookies_websites:
                self.driver.get(website)
                for cookie in cookies:
                    self.driver.add_cookie(cookie)
                self.driver.refresh()
        except Exception as e:
            # it'll fail for the first time, when cookie file is not present
            print(str(e))
            print("Error loading cookies")

    def app_login(self, username, password):
        self.driver.find_element_by_xpath("//*[@id='email']").send_keys(username)
        self.driver.find_element_by_xpath("//*[@id='password']").send_keys(password)
        self.driver.find_element_by_xpath("/html/body/div[1]/div/div[2]/div[2]/div[1]/form/div[5]/button").click()
        # return driver

if __name__ == '__main__':

    username = os.getenv('APP_USERNAME')
    password = os.getenv('APP_PASSWORD')
    app_object = AppLogin()
    driver = app_object.driver

    # if os.path.exists("./tmp/chrome/cookies.pkl"):
    #     app_object.load_cookies()
    # else:
    #     driver.get("https://portal.create.tv")
    #     app_object.app_login(username, password)
    #     app_object.save_cookies()
    #     app_object.load_cookies()
