import unittest
from selenium import webdriver

class SearchText(unittest.TestCase):
    def setUp(self):
        print("running before test")
        # create a new Firefox session
        # self.driver = webdriver.Firefox()

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--headless")

        # self.driver = webdriver.Chrome(
        #     executable_path="/home/chrisng/.nvm/node_modules/.bin/chromedriver"
        # )
        self.driver = webdriver.Remote(
            command_executor='http://127.0.0.1:4444/wd/hub',
            options=chrome_options)

        self.driver.implicitly_wait(10)
        # self.driver.maximize_window()
        # navigate to the application home page
        self.driver.get("http://www.google.com/")

    def test_search_by_text(self):
        # get the search textbox
        self.search_field = self.driver.find_element_by_name("q")

        # enter search keyword and submit
        self.search_field.send_keys("Selenium WebDriver Interview questions")
        self.search_field.submit()

        #get the list of elements which are displayed after the search
        #currently on result page usingfind_elements_by_class_namemethod

        lists = self.driver.find_elements_by_class_name("g")
        no=len(lists)
        self.assertEqual(15, len(lists))

    def tearDown(self):
        print("running after test")
        # close the browser window
        self.driver.quit()

if __name__ == '__main__':
    unittest.main()