# docker run -d -p 4444:4444 --shm-size="2g" selenium/standalone-chrome:latest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import os
import unittest
import portal_functions

class PortalTesting(unittest.TestCase):

    # GoogleLogin Test
    def test_d_GoogleLogin(self):
        driver=portal_functions.Glogin()
        time.sleep(6)
        self.assertEqual(driver.current_url, portal_functions.url+'#')
        driver.close
        driver.quit
