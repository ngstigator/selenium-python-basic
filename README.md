# selenium-python-basic

Simple selenium test written in Python.

## Requirements

* Python  3.4+
* [ChromeDriver](https://chromedriver.chromium.org/downloads)
* Selenium

## Install Selenium
`pip install selenium`

## Install Chromedriver (local)
from `~/.nvm` run:

`npm install chromedriver`

## Install Chromedriver (remote)

`docker run -d -p 4444:4444 --shm-size="2g" selenium/standalone-chrome:latest`

## Virtual environment
Create virtual environment in project directory:
`python3 -m venv ENV`

## Usage
### Activate virtual environment
`source ENV/bin/activate`

### Use VS Code to run test
Right click on test file and select `Run Python file in terminal`

### Cleanup
`deactivate`