import unittest
from TestBase import TestBase

class TestChild(TestBase):
    # def __init__(self):
    #     super().__init__()

    def setUp(self):
        print("running before test")

    def dummyChild(self):
        TestBase.dummyParent(self)

    def tearDown(self):
        print("running after test")

if __name__ == '__main__':
    unittest.main()
    # obj = TestChild()
    # obj.dummyChild()